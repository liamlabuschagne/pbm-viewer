from tkinter import *
master = Tk()

canvas_width = 500
canvas_height = 500
w = Canvas(master, width=canvas_width, height=canvas_height)
w.pack()

scale = 10
if len(sys.argv) < 2:
    print("No file specified")
    exit()

f = open(sys.argv[1], 'r')
data = f.read()
data = data.split('\n')
if data[0] == "P1":
    iw = int(data[1].split(' ')[0])
    ih = int(data[1].split(' ')[1])

    print("Image is "+str(iw)+"x"+str(ih))

    offsetx = canvas_width/2-(iw*scale/2)
    offsety = canvas_height/2-(ih*scale/2)

    imagedata = data[2:]

    for i in range(ih):
        row = imagedata[i].split(' ')
        for j in range(iw):
            if int(row[j]) == 1:
                w.create_rectangle(j*scale+offsetx, i*scale+offsety, j *
                                   scale+scale+offsetx, i*scale+scale+offsety, fill='black')

f.close()
mainloop()
